import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * Connect4 JavaFX GUI Implementation
 */
public class ConnectFour extends Application {
    private GameBoard board = new GameBoard();
    private Label endGameStatus = new Label();  // Reports winner, else lack of one

    @Override
    public void start(Stage primaryStage) {
        BorderPane pane = new BorderPane();
        pane.setCenter(board);
        pane.setBottom(endGameStatus);

        BorderPane.setAlignment(endGameStatus, Pos.CENTER);
        endGameStatus.setFont(new Font("Verdana", 30));

        Scene scene = new Scene(pane);
        primaryStage.setTitle("Connect4");
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.show();
    }

    /**
     * Gameboard pane implements all gameboard objects and game logic
     */
    public class GameBoard extends GridPane {
        private int maxRows = 6;
        private int maxCols = 7;
        private Circle[][] tokenSlots = new Circle[maxRows][maxCols]; // Used for easier circle handling
        private int[][] coords = null;
        private int tokenCount = 0;   // Counts how many tokens are placed in board
        private int maxTokens = maxRows * maxCols;  // Game ends when tokenCount == maxTokens
        private String whoseTurn = "Red";   // Starting player = red
        private boolean areFourConnected = false;

        GameBoard() {
            //Pane formatting
            setStyle("-fx-background-color: grey");
            setPadding(new Insets(10));
            setHgap(5);
            setVgap(5);

            //Add all circles to board
            for (int i = 0; i < maxCols; i++) {
                for (int j = 0; j < maxRows; j++) {
                    Circle circle = new Circle(25, Color.WHITE);    //Empty spots represented by white circles
                    circle.setStroke(Color.BLACK);
                    tokenSlots[j][i] = circle;
                    add(circle, i, j);
                    final int col = i;
                    final int row = j;
                    circle.setOnMouseClicked(e -> handleMouseClick(circle, col, row));
                }
            }
        }

        /**
         * Handles token insertions and end of game logic
         * @param circle most recently placed token
         * @param col of most recently placed token
         * @param row of most recently placed token
         */
        private void handleMouseClick(Circle circle, int col, int row) {
            //If position is available and game isn't over
            if ((!areFourConnected && tokenCount <= maxTokens)) {
                if ((circle.getFill() == Color.WHITE)) {
                    if (((row == (maxRows - 1)) ||
                            (tokenSlots[row + 1][col].getFill() != Color.WHITE))) {

                        //Insert colored token
                        circle.setFill(whoseTurn.equals("Red")? Color.RED : Color.BLACK);
                        tokenCount++;

                        // Check if game is won or a draw has occurred; If not, continue playing
                        coords = areFourConnected(tokenSlots);
                        {
                            // Coords will be null as long as game isn't won
                            if (coords != null) {
                                areFourConnected = true;
                            }

                            if (!areFourConnected && tokenCount < maxTokens) {
                                //Change turns
                                whoseTurn = (whoseTurn.equals("Red")) ? "Black" : "Red";
                            } else if (areFourConnected) {
                                flashTokens();  // Causes winning tokens to flash green
                                endGameStatus.setText("Connect4! " + whoseTurn + " wins!");
                            } else {
                                endGameStatus.setText("Draw :(");
                            }
                        }
                    }
                }
            }
        }

        /**
         * Checks horizontal, vertical, and diagonal win cases
         * @param tokenSlots holds circle positions on GameBoard
         * @return winning token coordinates, else null
         */
        private int[][] areFourConnected(Circle[][] tokenSlots) {
            int rows = tokenSlots.length;
            int cols = tokenSlots[0].length;

            // Horizontal win case
            for (int i = 0; i < rows; i++) {
                int k = areFourConnected(tokenSlots[i]);    // Starting column of 4 in a row

                // If solution is found in row
                if (k != -1) {
                    int[][] coords = new int[4][2];     // Solution coordinates

                    coords[0][0] = i; coords[0][1] = k;
                    coords[1][0] = i; coords[1][1] = k + 1;
                    coords[2][0] = i; coords[2][1] = k + 2;
                    coords[3][0] = i; coords[3][1] = k + 3;

                    return coords;
                }
            }

            // Vertical win case
            for (int i = 0; i < cols; i++) {
                Circle[] column = new Circle[rows];

                // Store column i in array
                for (int j = 0; j < rows; j++)
                    column[j] = tokenSlots[j][i];

                int k = areFourConnected(column);   // Starting row of 4 in a row

                // If solution is found in column
                if (k != -1) {
                    int[][] coords = new int[4][2]; // Solution coordinates

                    coords[0][1] = i; coords[0][0] = k;
                    coords[1][1] = i; coords[1][0] = k + 1;
                    coords[2][1] = i; coords[2][0] = k + 2;
                    coords[3][1] = i; coords[3][0] = k + 3;

                    return coords;
                }
            }

            // Top-left to bottom-right diagonal win case (lower half)
            for (int i = 0; i < rows - 3; i++) {
                int diagonalElements = Math.min(rows - i, cols);
                Circle[] diagonal = new Circle[diagonalElements];

                // Store diagonal in array
                for (int j = 0; j < diagonalElements; j++) {
                    diagonal[j] = tokenSlots[j + i][j];
                }

                int k = areFourConnected(diagonal); // Starting diagonal of solution

                // Return solution coordinates if solution is found
                if (k != -1) {
                    int[][] coords = new int[4][2]; // Solution coordinates

                    coords[0][0] = k + i;     coords[0][1] = k;
                    coords[1][0] = k + 1 + i; coords[1][1] = k + 1;
                    coords[2][0] = k + 2 + i; coords[2][1] = k + 2;
                    coords[3][0] = k + 3 + i; coords[3][1] = k + 3;

                    return coords;
                }
            }

            // Top-left to bottom-right diagonal win case (upper half)
            for (int i = 1; i < cols - 3; i++) {
                int diagonalElements = Math.min(cols - i, rows);
                Circle[] diagonal = new Circle[diagonalElements];

                // Store diagonals in array
                for (int j = 0; j < diagonalElements; j++)
                    diagonal[j] = tokenSlots[j][j + i];

                int k = areFourConnected(diagonal); // Starting diagonal of solution

                // Return solution coordinates if solution is found
                if (k != -1) {
                    int[][] coords = new int[4][2]; // Solution coordinates

                    coords[0][0] = k;     coords[0][1] = k + i;
                    coords[1][0] = k + 1; coords[1][1] = k + 1 + i;
                    coords[2][0] = k + 2; coords[2][1] = k + 2 + i;
                    coords[3][0] = k + 3; coords[3][1] = k + 3 + i;

                    return coords;
                }
            }

            // Top-right to bottom-left diagonal win case (upper half)
            for (int i = 3; i < cols; i++) {
                int diagonalElements = Math.min(i + 1, rows);
                Circle[] diagonal = new Circle[diagonalElements];

                // Store diagonals in array
                for (int j = 0; j < diagonalElements; j++) {
                    diagonal[j] = tokenSlots[j][i - j];
                }

                int k = areFourConnected(diagonal); // Starting diagonal of solution

                // Return solution coordinates if solution is found
                if (k != -1) {
                    int[][] coords = new int[4][2]; // Solution coordinates

                    coords[0][0] = k;     coords[0][1] = i - k;
                    coords[1][0] = k + 1; coords[1][1] = i - k - 1;
                    coords[2][0] = k + 2; coords[2][1] = i - k - 2;
                    coords[3][0] = k + 3; coords[3][1] = i - k - 3;

                    return coords;
                }
            }

            // Top-right to bottom-left diagonal win case (lower half)
            for (int i = 1; i < rows - 3; i++) {
                int diagonalElements = Math.min(rows - i, cols);
                Circle[] diagonal = new Circle[diagonalElements];

                // Store diagonals in array
                for (int j = 0; j < diagonalElements; j++) {
                    diagonal[j] = tokenSlots[j + i][cols - j - 1];
                }

                int k = areFourConnected(diagonal); // Starting diagonal of solution

                // Return solution coordinates if solution is found
                if (k != -1) {
                    int[][] coords = new int[4][2]; // Solution coordinates

                    coords[0][0] = k + i;     coords[0][1] = cols - k - 1;
                    coords[1][0] = k + i + 1; coords[1][1] = cols - (k + 1) - 1;
                    coords[2][0] = k + i + 2; coords[2][1] = cols - (k + 2) - 1;
                    coords[3][0] = k + i + 3; coords[3][1] = cols - (k + 3) - 1;

                    return coords;
                }
            }

            return null;
        }

        /**
         * areFourConnected overloaded helper function
         * Given a 1D array returns the index of the first token in the 4 in a row
         * @param tokenSlots array of tokens to check
         * @return index of first token in 4 in a row
         */
        private int areFourConnected(Circle[] tokenSlots) {
            for (int i = 0; i < tokenSlots.length - 3; i++) {
                boolean isEqual = true;
                for (int j = i; j < i + 3; j++) {
                    if (tokenSlots[j].getFill() == Color.WHITE || tokenSlots[j].getFill() != tokenSlots[j + 1].getFill()) {
                        isEqual = false;
                        break;
                    }
                }

                if (isEqual) {
                    return i;
                }
            }

            // Return -1 if the passed tokens do not include 4 in a row
            return -1;
        }

        /**
         * Handles animation logic for flashing tokens in case of win
         */
        private void flashTokens() {
            Timeline animation = new Timeline(new KeyFrame(Duration.millis(250), e -> {
                if (tokenSlots[coords[0][0]][coords[0][1]].getStroke() == Color.LIMEGREEN) {
                    for (int[] coord : coords) {
                        tokenSlots[coord[0]][coord[1]].setStroke(Color.BLACK);
                    }
                } else {
                    for (int[] coord : coords) {
                        tokenSlots[coord[0]][coord[1]].setStroke(Color.LIMEGREEN);
                    }
                }

                for (int i = 0; i < 10; i++) {
                    endGameStatus.setTextFill(Color.rgb((int)(Math.random() * 255), (int)(Math.random() * 255), (int)(Math.random() * 255)));
                }
            }));

            animation.setCycleCount(11);
            animation.play();
        }
    }
}
